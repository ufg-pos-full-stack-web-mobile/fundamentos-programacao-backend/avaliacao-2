package br.ufg.pos.fswm.fpb.avaliacao2.dao;

import br.ufg.pos.fswm.fpb.avaliacao2.modelo.Produto;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public class ProdutoDAOImpl implements ProdutoDAO{
    @Override
    public void salvar(Produto produto) {
        EntityManager em = PersistenceManager.getInstanceOf().getEntityManager();
        em.getTransaction().begin();
        em.persist(produto);
        em.getTransaction().commit();
    }

    @Override
    public Produto buscarPorId(Integer id) {
        EntityManager em = PersistenceManager.getInstanceOf().getEntityManager();

        final String hql = "SELECT bean FROM Produto bean WHERE bean.id = :id";

        Query query = em.createQuery(hql);
        query.setParameter("id", id);

        try {
            return (Produto) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Produto> buscarTodos() {
        EntityManager em = PersistenceManager.getInstanceOf().getEntityManager();

        final String hql = "SELECT bean FROM Produto bean";

        Query query = em.createQuery(hql);

        return (List<Produto>) query.getResultList();
    }

    @Override
    public void atualizar(Produto produto) {
        EntityManager em = PersistenceManager.getInstanceOf().getEntityManager();
        em.getTransaction().begin();
        em.merge(produto);
        em.getTransaction().commit();
    }

    @Override
    public void excluir(Integer id) {
        Produto produto = buscarPorId(id);

        EntityManager em = PersistenceManager.getInstanceOf().getEntityManager();

        em.getTransaction().begin();
        produto = em.merge(produto); // colocando dentro do contexto de persistencia
        em.remove(produto);
        em.getTransaction().commit();
    }
}
