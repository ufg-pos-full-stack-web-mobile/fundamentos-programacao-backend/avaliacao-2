package br.ufg.pos.fswm.fpb.avaliacao2.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/07/17.
 */
public class PersistenceManager {

    private static PersistenceManager instance;

    private EntityManagerFactory emFactory;

    private PersistenceManager() {
        // "jpa-example" was the value of the name attribute of the
        // persistence-unit element.
        emFactory = Persistence.createEntityManagerFactory("jpa-example");
    }

    public static PersistenceManager getInstanceOf() {
        if(instance == null) {
            instance = new PersistenceManager();
        }
        return instance;
    }

    public EntityManager getEntityManager() {
        return emFactory.createEntityManager();
    }

    public void close() {
        emFactory.close();
    }

}
